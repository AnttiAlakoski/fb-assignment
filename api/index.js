'use strict';

const movieController = require('../controllers/movie.controller');

module.exports = function (server){
  // Movies endpoints for test-assignment
  server.get('/api/movies', movieController.getMovies);
  server.get('/api/movies/:id', movieController.getMovie);
};