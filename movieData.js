'use strict';

// dummy list of movie ids
const movieIds = [
  '5a2a7706b46dfa97320773f6','57b5a4057213de2831dc1b42','57b5a4087213de2831dc1b55',
  '57b6f08b7213de2831dc1c9d','57b6f0847213de2831dc1c8e','57bd2d9071cfba3367ed833c',
  '57bd2da371cfba3367ed8347','57bd484fac64edf86e6f5dbc','57bd484dac64edf86e6f5db0',
  '57bd51a1ac64edf86e6f5f11','57bd518cac64edf86e6f5f05','57bd532cac64edf86e6f5f98',
  '57bd531eac64edf86e6f5f74','57bd5774ac64edf86e6f60a5','57bd576eac64edf86e6f6095',
  '57bd5774ac64edf86e6f60b9','57bd57b4ac64edf86e6f60cf','57bd59c1ac64edf86e6f613f',
  '57bd59b3ac64edf86e6f612e','57c5644e3294aed97e9091d8','57c3d04d3294aed97e905f1c',
  '57c3d04d3294aed97e905f1d','57c3c9353294aed97e905e55','57c338c73294aed97e904fb8',
  '57c12e773294aed97e9018c0','57c335e73294aed97e904f66','57c12ff03294aed97e901935',
  '57c184ba3294aed97e9022fc','57c130053294aed97e901945','57c12f8c3294aed97e90192f',
  '57c12f8c3294aed97e90192e','57c12f8c3294aed97e90192d','57bed651ac64edf86e6f8a99',
  '57bea22dac64edf86e6f8318','57bde85aac64edf86e6f7012','57bde84aac64edf86e6f6ffe',
  '57bde85aac64edf86e6f7000','57bde30dac64edf86e6f6f6b'
];

// dummy movies to return
const movies = {
  '5a2a7706b46dfa97320773f6': {
    name: 'The Shawshank Redemption',
    year: 1994,
    rating: 9.2
  },
  '57b5a4057213de2831dc1b42': {
    name: 'The Godfather',
    year: 1972,
    rating: 9.2
  },
  '57b5a4087213de2831dc1b55': {
    name: 'The Godfather: Part II',
    year: 1974,
    rating: 9.2
  },
  '57b6f08b7213de2831dc1c9d': {
    name: 'The Dark Knight',
    year: 2008,
    rating: 9.1
  },
  '57b6f0847213de2831dc1c8e': {
    name: '12 Angry Men',
    year: 1957,
    rating: 8.9
  },
  '57bd2d9071cfba3367ed833c': {
    name: 'Schindler\'s List',
    year: 1993,
    rating: 8.9
  },
  '57bd2da371cfba3367ed8347': {
    name: 'Pulp Fiction',
    year: 1994,
    rating: 8.9
  },
  '57bd484fac64edf86e6f5dbc': {
    name: 'The Lord of the Rings: The Return of the King',
    year: 2003,
    rating: 8.9
  },
  '57bd484dac64edf86e6f5db0': {
    name: 'The Good, the Bad and the Ugly',
    year: 1966,
    rating: 8.8
  },
  '57bd51a1ac64edf86e6f5f11': {
    name: 'Fight Club',
    year: 1999,
    rating: 8.8
  },
  '57bd518cac64edf86e6f5f05': {
    name: 'The Lord of the Rings: The Fellowship of the Ring',
    year: 2001,
    rating: 8.8
  },
  '57bd532cac64edf86e6f5f98': {
    name: 'Forrest Gump',
    year: 1994,
    rating: 8.7
  },
  '57bd531eac64edf86e6f5f74': {
    name: 'Star Wars: Episode V - The Empire Strikes Back',
    year: 1980,
    rating: 8.7
  },
  '57bd5774ac64edf86e6f60a5': {
    name: 'Inception',
    year: 2010,
    rating: 8.7
  },
  '57bd576eac64edf86e6f6095': {
    name: 'The Lord of the Rings: The Two Towers',
    year: 2002,
    rating: 8.7
  },
  '57bd5774ac64edf86e6f60b9': {
    name: 'One Flew Over the Cuckoo\'s Nest',
    year: 1975,
    rating: 8.7
  },
  '57bd57b4ac64edf86e6f60cf': {
    name: 'Goodfellas',
    year: 1990,
    rating: 8.7
  },
  '57bd59c1ac64edf86e6f613f': {
    name: 'The Matrix',
    year: 1999,
    rating: 8.7
  },
  '57bd59b3ac64edf86e6f612e': {
    name: 'Seven Samurai',
    year: 1954,
    rating: 8.6
  },
  '57c5644e3294aed97e9091d8': {
    name: 'Star Wars: Episode IV - A New Hope',
    year: 1977,
    rating: 8.6
  },
  '57c3d04d3294aed97e905f1c': {
    name: 'City of God',
    year: 2002,
    rating: 8.6
  },
  '57c3d04d3294aed97e905f1d': {
    name: 'Se7en',
    year: 1995,
    rating: 8.6
  },
  '57c3c9353294aed97e905e55': {
    name: 'The Silence of the Lambs',
    year: 1991,
    rating: 8.6
  },
  '57c338c73294aed97e904fb8': {
    name: 'It\'s a Wonderful Life',
    year: 1946,
    rating: 8.6
  },
  '57c12e773294aed97e9018c0': {
    name: 'Life Is Beautiful',
    year: 1997,
    rating: 8.6
  },
  '57c335e73294aed97e904f66': {
    name: 'The Usual Suspects',
    year: 1995,
    rating: 8.6
  },
  '57c12ff03294aed97e901935': {
    name: 'Léon: The Professional',
    year: 1994,
    rating: 8.5
  },
  '57c184ba3294aed97e9022fc': {
    name: 'Saving Private Ryan',
    year: 1998,
    rating: 8.5
  },
  '57c130053294aed97e901945': {
    name: 'Spirited Away',
    year: 2001,
    rating: 8.5
  },
  '57c12f8c3294aed97e90192f': {
    name: 'American History X',
    year: 1998,
    rating: 8.5
  },
  '57c12f8c3294aed97e90192e': {
    name: 'Once Upon a Time in the West',
    year: 1968,
    rating: 8.5
  },
  '57c12f8c3294aed97e90192d': {
    name: 'Interstellar',
    year: 2014,
    rating: 8.5
  },
  '57bed651ac64edf86e6f8a99': {
    name: 'The Green Mile',
    year: 1999,
    rating: 8.5
  },
  '57bea22dac64edf86e6f8318': {
    name: 'Psycho',
    year: 1960,
    rating: 8.5
  },
  '57bde85aac64edf86e6f7012': {
    name: 'Casablanca',
    year: 1942,
    rating: 8.5
  },
  '57bde84aac64edf86e6f6ffe': {
    name: 'City Lights',
    year: 1931,
    rating: 8.5
  },
  '57bde85aac64edf86e6f7000': {
    name: 'The Intouchables',
    year: 2011,
    rating: 8.5
  },
  '57bde30dac64edf86e6f6f6b': {
    name: 'Modern Times',
    year: 1936,
    rating: 8.5
  }
};

module.exports = {
  movieIds,
  movies
};