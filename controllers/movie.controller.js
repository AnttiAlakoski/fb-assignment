'use strict';

const { movieIds } = require('../movieData');
const { movies } = require('../movieData');

module.exports = {
  getMovies,
  getMovie
};

function getMovies(req, res) {
  return res.send({movies: movieIds});
}

function getMovie(req, res) {
  const id = req.params.id;

  const movie = movies[id];

  if (!movie) {
    return res.send(404, {message: 'Not found.'});
  }
  
  return res.send(movie);
}

