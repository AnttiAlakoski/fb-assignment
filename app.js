'use strict';

const restify = require('restify');
const api = require('./api/index');
const config = require('./config');

const server = restify.createServer({
  name: config.name,
  version: config.version
});

//initial restify server settings
server.pre(restify.CORS());
restify.CORS.ALLOW_HEADERS.push('authorization');
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser({
  mapParams: false
}));
server.use(restify.fullResponse());

//API Routes
api(server);

server.listen(config.port, function () {
  console.log(`${server.name} running on port: ${config.port}`);
});